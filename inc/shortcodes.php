<?php

/*
Usage: Shortcodes
Author: CodeDragon
Author URI: https://www.codedragon.ca
*/

/**
 * Hero image shortcode
 * @param  array  $atts [description]
 * @return html
 */
function show_gallery_hero($atts = array()){
	$atts = shortcode_atts(array(
    ), $atts, 'show_gallery_hero');

	// get ACF field data in desired order for hero
	$soliloquy_slider = get_field('soliloquy_slider');
	$wp_videos = get_field('wp_videos');
	$yt_vimeo_video = get_field('yt_vimeo_video');
	if(!empty($soliloquy_slider)){
		// Soliloquy
		$html = do_shortcode('[soliloquy id="' . $soliloquy_slider->ID . '"]');
	}elseif(!empty($yt_vimeo_video)){
		// Youtube/Vimeo
		$video_key = array_rand($yt_vimeo_video, 1);
		$video = $yt_vimeo_video[$video_key]['gallery_video'];
		$title = $yt_vimeo_video[$video_key]['gallery_title'];
		if(strpos($video, 'youtu') !== false){
			$html = do_shortcode('[vidbg container="#shortcode-3-8814" type="youtube" youtube_url="' . $video . '" frontend_play_button="true" frontend_volume_button="true"]');
		}else{
			$html = do_shortcode('[vidbg container="#shortcode-3-8814" type="vimeo" vimeo_url="' . $video . '" frontend_play_button="true" frontend_volume_button="true"]');
		}
	}elseif(!empty($wp_videos)){
		// WordPress
		$video_key = array_rand($wp_videos, 1);
		$video = wp_get_attachment_url($wp_videos[$video_key]);
		$html = do_shortcode('[vidbg container="#shortcode-3-8814" mp4="' . $video . '" frontend_play_button="true" frontend_volume_button="true"]');
	}
	return $html;
}
add_shortcode('gallery_hero', 'show_gallery_hero');

/**
 * Before and after slider
 * @param  array  $atts
 * @return html
 */
function show_before_after_slider($atts = array()){
	global $post;

	$atts = shortcode_atts(array(
		'direction'		=> 'horizontal',
		'align'			=> 'left',
		'before-label'	=> '',
		'after-label'	=> '',
		'offset'		=> '0.5',
		'hover-slide'	=> 'false',
		'width'			=> '100%',
		'fieldset'		=> 'before_after_image',
		'field1'		=> 'before',
		'field2'		=> 'after'
    ), $atts, 'show_before_after_slider');

	$html = '';
	$images = get_field($atts['fieldset']);
	if(!empty($images[0][$atts['field1']]) && !empty($images[0][$atts['field2']])){
		if(empty($atts['before-label'])){
			$atts['before-label'] = $images[0][$atts['field1']]['caption'];
			if(empty($atts['before-label']))
				$atts['before-label'] = 'Before...';
		}
		if(empty($atts['after-label'])){
			$atts['after-label'] = $images[0][$atts['field2']]['caption'];
			if(empty($atts['after-label']))
				$atts['after-label'] = 'After...';
		}
		$html = do_shortcode('[twenty20 img1="' . $images[0][$atts['field1']]['ID'] . '" img2="' . $images[0][$atts['field2']]['ID']. '" direction="' . $atts['direction'] . '" offset="' . $atts['offset'] . '" align="' . $atts['align'] . '" width="' . $atts['width'] . '" before="' . $atts['before-label'] . '" after="' . $atts['after-label'] . '" hover="' . $atts['hover-slide'] . '"]');
	}
	return $html;
}
add_shortcode('ba_slider', 'show_before_after_slider');

/**
 * Artist profile photo
 * @return html
 */
function show_artist_image($atts = array()){
    global $post;

	$atts = shortcode_atts(array(
		'type' => 'pro'
    ), $atts, 'artist_profile_photo');

    $html = '';
	if($atts['type'] == 'featured'){
		$image = get_the_post_thumbnail_url( $post, 'large' );
	    if(!empty($image)){
	    	$html = '<div class="profile_photo_featured" style="background-image: url(' . $image . ');"></div>';
	    }
	}else{
	    $image = get_field('profile_photos');
	    if(!empty($image)){
	    	$html = '<div class="profile_photo_' . $atts['type'] . '" style="background-image: url(' . $image[0][$atts['type']]['url'] . ');"></div>';
	    }
	}

    return $html;
}
add_shortcode('artist_profile_photo', 'show_artist_image');
