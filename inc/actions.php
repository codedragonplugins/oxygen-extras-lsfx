<?php

/// Add Actions

/**
 * Enqueue scripts/stylesheets
 */
if ( ! function_exists( 'enqueue_oxyex_scripts' ) ) {
    function enqueue_oxyex_scripts() {
        if(!is_admin()) {
            global $post;

            // scripts
            wp_enqueue_script( 'font-awesome-latest', '//use.fontawesome.com/releases/v5.3.1/js/all.js', array(), null );

            // styles
            wp_enqueue_style( 'font-awesome-latest-css', 'https://use.fontawesome.com/releases/v5.5.0/css/all.css' );

            // AJAX
            wp_localize_script(
                'oxyex-script',
                'oxyex_ajax_obj',
                array(
                    'ajaxurl' => admin_url('admin-ajax.php'),
                    'nonce' => wp_create_nonce('oxyex-ajax-nonce')
                )
            );
        }
    }
}
add_action( 'wp_enqueue_scripts', 'enqueue_oxyex_scripts' );

/**
 * Enqueue Font Awesome.
 */
function oxyex_load_admin_scripts() {
    wp_register_style( 'oxyex-wp-admin-css', plugins_url( 'oxygen-extras-lsfx/assets/css/admin.css' ) );
    wp_enqueue_style( 'oxyex-wp-admin-css' );

    wp_enqueue_script( 'oxyex-wp-admin-scripts', plugins_url( 'oxygen-extras-lsfx/assets/js/admin.js' ), array(), null );
}
add_action( 'admin_enqueue_scripts', 'oxyex_load_admin_scripts' );

add_theme_support( 'post-thumbnails', array( 'post' ) );

/**
 * Ajax calls
 */
function oxyex_ajax_request() {
    $nonce = $_POST['nonce'];

    if (!wp_verify_nonce( $nonce, 'oxyex-ajax-nonce')) {
        die('Nonce value cannot be verified.');
    }

    // The $_REQUEST contains all the data sent via ajax
    if (isset($_REQUEST['request'])) {
        global $wp_query;

        switch($_REQUEST['request']) {
            case '':
                // wp_send_json_success($data, 200);
                break;
        }
    }

    die();
}
add_action('wp_ajax_oxyex_ajax_request', 'oxyex_ajax_request');
add_action('wp_ajax_nopriv_oxyex_ajax_request', 'oxyex_ajax_request');

/**
 * Allow to override WC /templates path
 */
function oxyex_after_setup_oxyb() {
    add_filter( 'wc_get_template', 'oxyex_wc_template', 10, 5 );
    add_filter( 'wc_get_template_part', 'oxyex_wc_template_part', 10, 3 );
}
add_action( 'after_setup_theme', 'oxyex_after_setup_oxyb' );

/**
 * Woocommerce templates override
 * @param  string $located
 * @param  string $template_name
 * @param  array $args
 * @param  string $template_path
 * @param  string $default_path
 * @return string
 */
function oxyex_wc_template( $located, $template_name, $args, $template_path, $default_path ) {
    $newtpl = str_replace( 'woocommerce/templates', OXYEX_ROOT . '/woocommerce', $located );

    if ( file_exists( $newtpl ) )
        $located = $newtpl;

    return $located;
}

/**
 * Woocommerce templates part override
 * @param  string $template
 * @param  string $slug
 * @param  string $name
 * @return string
 */
function oxyex_wc_template_part( $template, $slug, $name ) {
    $newtpl = str_replace( 'woocommerce/templates', OXYEX_ROOT . '/woocommerce', $template );

    if ( file_exists( $newtpl ) )
        $template = $newtpl;

    return $template;
}