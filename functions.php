<?php

/*
Plugin Name: Oxygen Extras
Author: CodeDragon
Author URI: https://www.codedragon.ca
Description: Functions that add unique operation to an Oxygen Builder site
Version: 1.0.8
Text Domain: codedragon
*/

if(!defined('OXYEX_ROOT'))           define('OXYEX_ROOT', plugin_dir_path(__FILE__));
if(!defined('OXYEX_URL'))            define('OXYEX_URL', plugin_dir_url(__FILE__));

include_once('inc/shortcodes.php');
include_once('inc/menu_class.php');

include_once("inc/filters.php");
include_once("inc/actions.php");

/**
 * Activation processes
 * @return
 */
function oxyex_activate() {
    if ( ! class_exists('OxyEl') ) {
        //* Deactivate ourself
        deactivate_plugins( __FILE__ );
        add_action( 'admin_notices',        'oxyex_admin_notice_message' );
        add_action( 'network_admin_notices',    'oxyex_admin_notice_message' );
        return;
    }
}

/**
 * Shows an admin notice if you're not using the Oxygen Builder Plugin.
 */
function oxyex_admin_notice_message() {
    if ( ! is_admin() ) {
        return;
    } else if ( ! is_user_logged_in() ) {
        return;
    } else if ( ! current_user_can( 'update_core' ) ) {
        return;
    }

    $error = __( 'Sorry, you can\'t use the Oxygen Extras plugin unless the Oxygen Builder Plugin is active. The plugin has been deactivated.', 'paulc-edit-oxybuilder' );

    echo '<div class="error"><p>' . $error . '</p></div>';

    if ( isset( $_GET['activate'] ) ) {
        unset( $_GET['activate'] );
    }
}

/**
 * Return plugin template sections
 * @param string $path
 * @param mixed $data
 * @return html
 */
if(!function_exists('oxyex_get_template_part')){
    function oxyex_get_template_part($path, $data = array()){
        global $_template_data;

        if(empty($path)) return null;

        $html = null;
        $_template_data = $data;
        if(file_exists(plugin_dir_path( __FILE__ ) . $path)){
            ob_start();
            include(plugin_dir_path( __FILE__ ) . $path);
            $html = ob_get_clean();
        }
        return $html;
    }
}

/**
 * Return ACF Gallery
 * @param  string $gallery_field [description]
 * @return html
 */
if(!function_exists('oxyex_get_acf_gallery')){
    function oxyex_get_acf_gallery($gallery_field = 'gallery'){
        $image_ids = get_field($gallery_field);
        oxyex_pr($image_ids);
        if( $image_ids ) {
            // Generate string of ids ("123,456,789").
            $images_string = implode( ',', $image_ids );

            // Generate and do shortcode.
            // Note: The following string is split to simply prevent our own website from rendering the gallery shortcode.
            $shortcode = sprintf( '[gallery ids="%s"]', esc_attr($images_string) );
            return do_shortcode( $shortcode );
        }
    }
}

/**
 * Form API Tokens
 * @param  boolean $thisHour
 * @return string
 */
if(!function_exists('get_token')){
    function get_token($thisHour = TRUE) {  // provides token to insert on page or to compare with the one from page
        if ($thisHour) $theHour = date("jH"); else $theHour = date("jH", time() - 3600); // token for current or previous hour
        return hash('sha256', 'salt__getoncom__key' . $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT'] .  $theHour);
    }
}

/**
 * Returns if token is valid
 * @param  string $token
 * @return boolean
 */
if(!function_exists('is_valid_token')){
    function is_valid_token($token) {  // is token valid for current or previous hour
        return (get_token() == $token || get_token(FALSE) == $token);
    }
}

/**
 * Return constants starting with string
 * @param  string $prefix
 * @return array
 */
if(!function_exists('return_constants')){
    function return_constants($prefix, $reverse_keys_values = false) {
        $dump = array();
        foreach (get_defined_constants() as $key => $value)
            if (substr($key, 0, strlen($prefix)) == $prefix){
                if($reverse_keys_values)
                    $dump[$value] = $key;
                else
                    $dump[$key] = $value;
            }
        return $dump;
    }
}

/**
 * Return 's if string does not end in one, other wise just a '
 * @param  [type] $text [description]
 * @return [type]       [description]
 */
function get_possessive($text){
    $text = preg_replace('/[^a-zA-Z0-9 -]/', '', $text);
    $text .= "'";
    if(substr($text, -1, 1) != 's') $text .= 's';
    return $text;
}

/**
 * Simple helper function for make menu item objects
 *
 * @param $title      - menu item title
 * @param $url        - menu item url
 * @param $order      - where the item should appear in the menu
 * @param int $parent - the item's parent item
 * @return \stdClass
 */
function do_custom_nav_menu_item( $title, $url, $order, $parent = 0 ){
    $item = new stdClass();
    $item->ID = 1000000 + $order + $parent;
    $item->db_id = $item->ID;
    $item->title = $title;
    $item->url = $url;
    $item->menu_order = $order;
    $item->menu_item_parent = $parent;
    $item->type = '';
    $item->object = '';
    $item->object_id = '';
    $item->classes = array();
    $item->target = '';
    $item->attr_title = '';
    $item->description = '';
    $item->xfn = '';
    $item->status = '';
    return $item;
}

/**
 * Return excerpt truncated to nearest word within limit
 * @param  integer $limit
 * @return string
 */
function get_excerpt($limit = 250){
    global $post;

    $limit = intval($limit);
    $excerpt = get_the_excerpt();
    $excerpt = substr($excerpt, 0, $limit);
    $result = substr($excerpt, 0, strrpos($excerpt, ' '));
    return $result;
}

/**
 * Get page by slug respecting polylang
 * @param  string $slug
 * @param  string $lang_slug
 * @param  string $type
 * @return string
 */
function get_link_by_slug($slug, $lang_slug = null, $type = 'post'){
    $post = get_page_by_path($slug, OBJECT, $type);
    $id = ($lang_slug) ? pll_get_post($post->ID, $lang_slug) : $post->ID;
    return get_permalink($id);
}

/**
 * Return array element or default if not exists
 * @param  array &$array
 * @param  string $element
 * @param  mixed $default
 * @return mixed
 */
function get_if_set($array, $element, $index = null, $default = null){
    $data = $default;
    if(isset($array[$element])) {
        $data = $array[$element];
        if(!is_null($index) && isset($data[$index]))
            $data = $data[$index];
    }
    return $data;
}

/**
 * Format string into phone number
 * @param  string $s
 * @return string
 */
function format_phone_number($s) {
    $rx = "/
        (1)?\D*     # optional country code
        (\d{3})?\D* # optional area code
        (\d{3})\D*  # first three
        (\d{4})     # last four
        (?:\D+|$)   # extension delimiter or EOL
        (\d*)       # optional extension
    /x";
    preg_match($rx, $s, $matches);
    if(!isset($matches[0])) return false;

    $country = $matches[1];
    $area = $matches[2];
    $three = $matches[3];
    $four = $matches[4];
    $ext = $matches[5];

    $out = "$three-$four";
    if(!empty($area)) $out = "$area-$out";
    if(!empty($country)) $out = "+$country-$out";
    if(!empty($ext)) $out .= "x$ext";

    return $out;
}

/**
 * Format string into date
 * @param  string $s
 * @return string
 */
function format_date($s) {
    $rx = "/
        (\d{4})[-\/]*(\d{2})[-\/]*(\d{2})$
    /x";
    preg_match($rx, $s, $matches);
    if(!isset($matches[0])) return false;

    $out = "$matches[1]-$matches[2]-$matches[3]";

    return $out;
}

/**
 * Return image URL from ACF field data
 * @param  object $image
 * @return html
 */
function get_acf_image($image, $size = 'thumbnail'){
    $html = '';
    if(is_array($image)){
        // Image variables.
        $url = $image['url'];
        $title = $image['title'];
        $alt = $image['alt'];
        $caption = $image['caption'];

        // Thumbnail size attributes.
        if(in_array($size, array('thumbnail', 'medium', 'large'))){
            $url = $image['sizes'][$size];
            $width = $image['sizes'][$size . '-width'];
            $height = $image['sizes'][$size . '-height'];
        }

        // Begin caption wrap.
        if($caption){
            $html .= '<div class="wp-caption">';
        }

        $html .= '<img src="' . esc_url($url) . '" alt="' . esc_attr($alt) . '" />';

        // End caption wrap.
        if($caption){
            $html .= '<p class="wp-caption-text">' . esc_html($caption) . '</p></div>';
        }
    }
    return $html;
}

/**
 * [oxyex_pr description]
 * @param  array  $array
 */
function oxyex_pr($array, $title = '', $silent = true){
    if(is_array($array)){
        if($silent){
            print "<!-- " . $title . ": " . print_r($array, true) . " -->";
        }else{
            print "<pre>" . $title . ": " . print_r($array, true) . "</pre>";
        }
    }
}
